package club;
import java.util.Scanner;
import club.Socio.Tipo;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) throws Exception {

        Scanner sc = new Scanner(System.in);
        int op;
        Club c = new Club();

        do{
            System.out.println("1. Afiliar un socio al club.");
            System.out.println("2. Registrar una persona autorizada por un socio.");
            System.out.println("3. Pagar una factura.");
            System.out.println("4. Registrar un consumo en la cuenta de un socio");
            System.out.println("5. Aumentar fondos de la cuenta de un socio");
            System.out.println("6. Salir");
            System.out.print("Ingrese una opcion: ");
            op = Integer.parseInt(sc.next());
            switch (op){
                case 1:{
                    c.afiliarSocio("1714045786","Juan",Tipo.VIP);
                }break;
                case 2:{
                    c.agregarAutorizadoSocio("1714048786","Mario");
                }break;
                case 3:{
                    c.pagarFacturaSocio("1714045786",256);
                }break;
                case 4:{
                    c.registrarConsumo("1714045786","Juan","Comida",25.5);
                }break;
                case 5:{
                    c.aumentarFondosSocio("1714045786",65);
                }break;
                case 6:{
                    System.out.println("Gracias!");
                }break;
                default:
                    System.out.println("opcion invalida");
            }

        }while(op!=6);

    }
}